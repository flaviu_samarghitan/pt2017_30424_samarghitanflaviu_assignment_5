package streams;

import org.joda.time.DateTime;

public class MonitoredData {
	
	public DateTime start,end;
	public String label;
	
	public MonitoredData(DateTime st,DateTime e,String l)
	{
		start=st;
		end=e;
		label=l;
	}
	
	public String getLabel()
	{
		return label;
	}
	
	public DateTime getStart()
	{
		return start;
	}
	
	public DateTime getEnd()
	{
		return end;
	}
	
	public boolean greaterFive()
	{
		if(end.getMillis()-start.getMillis()< 1000*60*5)
			return true;
		return false;
	}
}
