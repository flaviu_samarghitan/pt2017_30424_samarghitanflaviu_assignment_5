package streams;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;

import org.joda.time.DateTime;

public class Main {

	
	public static BufferedWriter bufS,bufT,bufF,bufFt;
	public static FileWriter fileS=null,fileT=null,fileF=null,fileFt=null;
	List<MonitoredData> ld;
	
	private static List<MonitoredData> read()
	{
		List<MonitoredData> ld=new ArrayList<MonitoredData>();
		Scanner in=null;
		try {
				in = new Scanner(new FileReader("activities.txt"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		String dateSt,dateEnd,hourSt,hourEnd,lbl;
		while(in.hasNext())
		{
			dateSt=in.next();
			hourSt=in.next();
			
			String dt=dateSt.concat("T"+hourSt);
			DateTime st=DateTime.parse(dt);
			
			dateEnd=in.next();
			hourEnd=in.next();
			String de=dateEnd.concat("T"+hourEnd);
			DateTime ds=DateTime.parse(de); 
		
			lbl=in.next();
			ld.add(new MonitoredData(st,ds,lbl));
		}
		return ld;
		
	}
	
	private static void write(String s,BufferedWriter bf)
	{
		try {
			bf.write(s);
			bf.newLine();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private static void close(FileWriter f,BufferedWriter bf)
	{
		try {
			bf.close();
			f.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String args[])
	{
		//Opening files and writes
		try {
			fileS=new FileWriter("Second.txt");
			fileT=new FileWriter("Third.txt");
			fileF=new FileWriter("Forth.txt");
			fileFt=new FileWriter("Fifth.txt");
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		bufS=new BufferedWriter(fileS);
		bufT=new BufferedWriter(fileT);
		bufF=new BufferedWriter(fileF);
		bufFt=new BufferedWriter(fileFt);
		
		//Reading
		List<MonitoredData> ld=read();
		
		//First exercise
		long n=ld.stream().filter(dt-> dt.start.dayOfYear()!=dt.end.dayOfYear()).map(dt->dt.start.dayOfYear()).distinct().count();
		System.out.println("First exercise:\n Number of days: "+n);
		
		
		//Second exercise
		Map<String,Long> mp=ld.stream().collect(Collectors.groupingBy(MonitoredData::getLabel,Collectors.counting()));
	
		write("Occurences of each action:",bufS);
		write("",bufS);
		
		mp.forEach((what,times)->{
						write(what+" = " +times,bufS);
						});
		
		close(fileS,bufS);
	    
		//Third exercise
	    Map<Integer,Map<String,Long>> mmp=ld.stream().collect(Collectors.groupingBy(a->{int t=a.start.getDayOfYear()-ld.get(0).start.getDayOfYear(); return t;},Collectors.groupingBy(MonitoredData::getLabel,Collectors.counting())));
	   
	    write("Activities on a daily basis:",bufT);
		write("",bufT);
	    
	    mmp.forEach((day,map)->
	    {
	    	write("",bufT);
	    	write("Day "+(day+1)+": ",bufT);
	    	write("",bufT);
	    	
	    	Iterator<Map.Entry<String,Long>> it = map.entrySet().iterator();
	    	while (it.hasNext()) {
	    		Map.Entry <String,Long>pair = (Map.Entry<String,Long>)it.next();
	    		
	    		write(pair.getKey() + " = " + pair.getValue(),bufT);
	    	}
	    });
	    close(fileT,bufT);
	    
	    //Forth exercise
	    Map<String,Long> tenH=ld.stream().collect(Collectors.groupingBy((MonitoredData::getLabel),Collectors.summingLong(q->{ long d=(q.getEnd().getMillis()-q.getStart().getMillis()); return d;})));
	    
	    write("Activities that last longer than 10 hours:",bufF);
		write("",bufF);
		
	    tenH.forEach((activity,time)->
	    {
	    	if(time>10*60*60*1000)
	    	{
	    		int hours=(int)(time/(60*60*1000));
	    		int minutes=(int)((time/(60*1000))%60);
	    		write("\n"+activity+":\nHours: "+hours+ " Minutes: "+minutes,bufF);
	    	}
	    });
	    
	    close(fileF,bufF);
	    
	    //Fifth exercise
	    
	    write("Activities that have a 90% monitoring samples less than 5 minutes: ",bufFt);
		write("",bufFt);
	    
	    Map<String,List<MonitoredData>> mpF=ld.stream().collect(Collectors.groupingBy(MonitoredData::getLabel));
	    Map<String,List<MonitoredData>> mpL=ld.stream().filter(MonitoredData::greaterFive).collect(Collectors.groupingBy(MonitoredData::getLabel));
	    
	    List<String> neglijable=new ArrayList<String>();
	    mpF.forEach((activity,times)->
	    			{
	    				if(times!=null && mpL.get(activity)!=null && 0.9*times.size()<mpL.get(activity).size())
	    				{
	    					neglijable.add(activity);
	    				}
	    			});	
	    neglijable.forEach((name)-> {write(name,bufFt);});
	    close(fileFt,bufFt);
	}
}
